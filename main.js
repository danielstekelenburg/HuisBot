"use strict";
const TeleBot = require('telebot');
var whitelist = require('./whitelist.js');
var CommandCenter = require('./CommandCenter.js');
var Config = require('./Config.js');
whitelist = new whitelist();
const bot = new TeleBot(Config.API_key);
CommandCenter = new CommandCenter(bot);

bot.on('text', (msg) => {
  var id = msg.from.id;

  if(whitelist.validate(id)){

  
CommandCenter.handle(msg);
}else{
  msg.reply.text("Je bent niet prominent genoeg om mij te bevelen.");
}
});

bot.start();
