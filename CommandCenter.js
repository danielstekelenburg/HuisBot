"use strict";
const fs = require('fs');

function CommandCenter(bot) {
    this.bot = bot;
    this.modules = [];
    fs.readdirSync('./modules/').forEach(file => {
        var MOD = require('./modules/' + file);
        MOD = new MOD(bot);
        this.modules.push(MOD);
    })
}
CommandCenter.prototype.match = function(mod, message) {

    var words = String(message.text.toLowerCase().replace(/[^0-9a-z ]/gi, '')).split(" ");
    for (var i = 0; i < mod.keywords.length; i++) {
        var entry = mod.keywords[i];
        var x = 0;
        for (var i2 = 0; i2 < words.length; i2++) {
            var entry2 = words[i2];
            if (entry == entry2) {
                x = 1;
                break;
            }
        };
        if (x == 0) {
            return false;
            break;
        }
    };
    return true;


}
CommandCenter.prototype.handle = function(msg) {
    var x = 0;

    for (var j = 0; j < this.modules.length; j++) {

        var mod = this.modules[j];

        if (this.match(mod, msg)) {
		
            mod.execute(msg);
            x = 1;
            break;
        }

    }
    if (x == 0) {
        msg.reply.text("Ik snap niet wat je zegt kerel.");
    }
}
module.exports = CommandCenter;
